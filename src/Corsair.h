/********************************************************************
*
*	CLASS		:: Corsair
*	DESCRIPTION	:: A game in which the player can traverse a galaxy, completing missions, upgrade
*					their vessel, and maybe even acquire tributes from planets.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 08 / 08
*
********************************************************************/

#ifndef CorsairH
#define CorsairH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEColour.h>
#include <DBEMath.h>

namespace DBE {
	class Camera;
}
class BaseNPC;
class InterfaceContainer;
class Landscape;
class Player;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum GameState {
	//GS_Init = 0,
	GS_Menu = 0,
	GS_ShipMissionSelect,
	GS_Play,
	GS_Pause,
	GS_LevelComplete,
	GS_LevelFailed,
	GS_Count,
};

struct Mission {
	char m_name[256];
	char m_desc[1024];
	s32 m_reward;
};


/*******************************************************************/
class Corsair : public DBE::App {
	public:
		/// Constructor.
		Corsair() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with any 3D rendering.
		void HandleRender3D( float deltaTime);
		/// Deals with any 2D rendering.
		void HandleRender2D( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

		/// Sets the new game state and performs any immediate operations.
		bool SetGameState( GameState s, bool onNextUpdate = false);
		/// Starts a new game.
		void StartNewGame();

		/// Displays the mission information about a specific mission.
		void DisplayMissionInfo( s32 i);
		/// Selects the mission to be played.
		void SelectMission();

	private:
		/// Sets objects' y position to that of the landscape.
		void StickObjectsToLandscape();
		/// Sets the camera's position based on the player's location.
		void SetCameraPosition();

		static s32 LuaFunction_NewGame();
		static s32 LuaFunction_Exit();
		static s32 LuaFunction_SelectMissionOne();
		static s32 LuaFunction_SelectMissionTwo();
		static s32 LuaFunction_SelectMissionThree();
		static s32 LuaFunction_SelectMission();
		static s32 LuaFunction_GoToShipMissionSelect();

		const char* m_windowTitle;
		bool m_wireframe;
		bool m_debugMode;

		GameState m_state;
		GameState m_stateNew;
		
		DBE::Camera* mp_cam;

		InterfaceContainer* mp_interface;

		Player* mp_player;

		Landscape*	mp_landscape;
		BaseNPC*	mp_target;

		Mission m_missions[3];
		s32		m_selectedMission;

		/// Private copy constructor to prevent multiple instances.
		Corsair( const Corsair&);
		/// Private assignment operator to prevent multiple instances.
		Corsair& operator=( const Corsair&);

};

APP_MAIN( Corsair, BLUE);

/*******************************************************************/
#endif	// #ifndef CorsairH