/********************************************************************
*
*	CLASS		:: Player
*	DESCRIPTION	:: Holds the information about the player.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 08 / 13
*
********************************************************************/

#ifndef PlayerH
#define PlayerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEMovable.h>

namespace DBE {
	class UITexture;
}
/********************************************************************
*	Defines and constants.
********************************************************************/
/// Enum for the various positions the Player can be in.
enum PlayerPosition {
	PP_Standing = 0,
	PP_Crouching,
	PP_Lying,
	PP_Count,
};


/*******************************************************************/
class Player : public DBE::Movable {
	public:
		/// Constructor.
		Player();
		/// Destructor.
		~Player();

		/// Updates the Player (call 'Update').
		bool OnUpdate( float deltaTime);
		/// Renders 2D aspects of the player (i.e. the sniper zoom overlay).
		void Render2D( float deltaTime);
		
		/// Gets the sensitivity of the mouse.
		float GetMouseSensitivity() const;
		/// Gets the player's current height.
		float GetHeight() const;
		/// Gets the player's movement speed.
		float GetMoveSpeed() const;
		/// Gets the player's position state.
		PlayerPosition GetPositionState() const;
		/// Gets whether the player is in 'sniper mode'.
		bool IsInSniperMode() const;
		/// Gets the amount of credits the player has.
		s32 GetCredits() const;

		/// Sets whether the player is running or not.
		void SetRunning( bool r);
		/// Sets the player's position.
		void SetPositionState( PlayerPosition s);
		/// Sets whether the player is in 'sniper mode'.
		void SetSniperMode( bool sm);
		/// Gives the player credits.
		void GiveCredits( s32 c);

		/// Resets the player's state regarding their position.
		void ResetState();
		
	private:
		float m_height;
		
		bool			m_running;
		PlayerPosition	m_posState;

		bool			m_sniperMode;
		DBE::UITexture*	mp_sniperOverlay;

		s32 m_credits;

		/// Private copy constructor to prevent accidental multiple instances.
		Player( const Player& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Player& operator=( const Player& other);
		
};

/*******************************************************************/
#endif	// #ifndef PlayerH
