/********************************************************************
*
*	CLASS		:: PerlinNoiseGenerator
*	DESCRIPTION	:: Generates an array of values based on the Perlin Noise algorithm (i.e. difference
*					clouds filter from Photoshop).
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 07 / 03
*	SOURCE		:: https://solarianprogrammer.com/2012/07/18/perlin-noise-cpp-11/
*
********************************************************************/

#ifndef PerlinNoiseGeneratorH
#define PerlinNoiseGeneratorH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <vector>

#include <DBETypes.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class PerlinNoiseGenerator {
	public:
		/// Constructor.
		PerlinNoiseGenerator( u32 seed);
		/// Destructor.
		~PerlinNoiseGenerator();
		
		double Noise( double x, double y, double z);
		
	private:
		double Fade( double t);
		double Lerp( double t, double a, double b);
		double Grad( s32 hash, double x, double y, double z);

		std::vector<s32> m_perm;
		
		/// Private copy constructor to prevent accidental multiple instances.
		PerlinNoiseGenerator( const PerlinNoiseGenerator& other);
		/// Private assignment operator to prevent accidental multiple instances.
		PerlinNoiseGenerator& operator=( const PerlinNoiseGenerator& other);
		
};

/*******************************************************************/
#endif	// #ifndef PerlinNoiseGeneratorH
