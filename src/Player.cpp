/********************************************************************
*	Function definitions for the Player class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Player.h"

#include <DBEApp.h>
#include <DBEFont.h>
#include <DBEUITexture.h>
#include <DBEUtilities.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

/// Constants for the player's height in various positions.
static const float gsc_heightPositions[] = {
	1.0f,
	gsc_heightPositions[PP_Standing] / 2.0f,
	gsc_heightPositions[PP_Standing] / 10.0f,
};

/// Constant for the speed at which the player moves between height positions.
static const float gsc_heightChangeSpeed = gsc_heightPositions[PP_Standing] / 10.0f;

/// Constant for the player's movement speed.
static const float gsc_moveSpeed = 0.1f;

/// Constants for the different speeds the player can move at.
static const float gsc_moveMultiplierRunning = 1.2f;
static const float gsc_moveMultiplierCrouch = 0.5f;
static const float gsc_moveMultiplierLying = 0.0f;

/// Constant for how many credits the player starts with.
static const s32 gsc_creditsStart = 1000;


/**
* Constructor.
*/
Player::Player()
	: m_height( gsc_heightPositions[PP_Standing])
	, m_running( false)
	, m_posState( PP_Standing)
	, m_sniperMode( false)
	, m_credits( gsc_creditsStart)
{
	mp_sniperOverlay = new UITexture( "res/textures/texture_sniperOverlay.dds", GET_APP()->GetSamplerState());
}

/**
* Destructor.
*/
Player::~Player() {
	SafeDelete( mp_sniperOverlay);
}

/**
* Updates the Player.
*
* @param deltaTime :: The time taken to render the previous frame.
*
* @return True if everything updated correctly.
*/
bool Player::OnUpdate( float deltaTime) {
	// Move towards the correct height if not already at it.
	if( m_height != gsc_heightPositions[m_posState]) {
		float dest( gsc_heightPositions[m_posState]);
		float dist( dest - m_height);

		if( DBE_Absf( dist) < gsc_heightChangeSpeed) {
			m_height = dest;
		}
		else {
			float change( gsc_heightChangeSpeed);

			if( dist < 0.0f)
				change *= -1.0f;

			m_height += change;
		}
	}

	// Clamp the rotation of the player otherwise they can look so far down that they start looking
	// backwards at an up-side-down world.
	static float clampValue( 80.0f);
	float rotX( this->GetRotation().GetX());
	rotX = Clamp<float>( rotX, DBE_ToRadians( (-clampValue) - rotX), DBE_ToRadians( clampValue - rotX));
	this->GetRotationPointer()->SetX( rotX);

	return true;
}

/**
* Renders 2D aspects of the player (i.e. the sniper zoom overlay).
*
* @param deltaTime :: The time taken to render the previous frame.
*/
void Player::Render2D( float deltaTime) {
	// Render the sniper zoom overlay if the player is in 'sniper mode'.
	if( m_sniperMode) {
		mp_sniperOverlay->Render( deltaTime);
		DEFAULT_FONT()->DrawString2D( Vec3( 0.0f), Vec3( 0.0f), 1.0f, nullptr, "+");
	}
}

/**
* Gets the sensitivity of the mouse. The return value is divided by the amount the mouse has moved
* to determine how much to rotate the player by. It should be lower in sniper mode to allow for
* better accuracy.
*
* @return The sensitivity of the mouse.
*/
float Player::GetMouseSensitivity() const {
	float s( 100.0f);

	if( this->IsInSniperMode())
		s *= 5.0f;

	return s;
}

/**
* Gets the player's current height.
*
* @return The player's current height.
*/
float Player::GetHeight() const {
	return m_height;
}

/**
* Gets the player's movement speed.
*
* @return The player's movement speed.
*/
float Player::GetMoveSpeed() const {
	float speed( gsc_moveSpeed);

	// Apply the multiplier for the player's position.
	switch( m_posState) {
		case PP_Crouching:	speed *= gsc_moveMultiplierCrouch;	break;
		case PP_Lying:		speed *= gsc_moveMultiplierLying;	break;
	}

	// apply the running multiplier if the player is currently running.
	if( m_running)
		speed *= gsc_moveMultiplierRunning;

	return speed;
}

/**
* Gets the player's position state.
*
* @return The player's position state.
*/
PlayerPosition Player::GetPositionState() const {
	return m_posState;
}

/**
* Gets whether the player is in 'sniper mode'.
*
* @return True if the player is in 'sniper mode'.
*/
bool Player::IsInSniperMode() const {
	return m_sniperMode;
}

/**
* Gets the amount of credits the player has.
*
* @return The number of credits the player has.
*/
s32 Player::GetCredits() const {
	return m_credits;
}

/**
* Sets whether the player is running or not.
*
* @param r :: True if the player is running.
*/
void Player::SetRunning( bool r) {
	m_running = r;
}

/**
* Sets the player's position.
*
* @param s :: The player's new position.
*/
void Player::SetPositionState( PlayerPosition s) {
	// Changing position takes the player out of sniper mode.
	this->SetSniperMode( false);

	if( m_posState == s) {
		switch( m_posState) {
			case PP_Crouching:		m_posState = PP_Standing;	break;
			case PP_Lying:			m_posState = PP_Standing;	break;
		}
	}
	else {
		m_posState = s;
	}
}

/**
* Sets whether the player is in 'sniper mode'.
*
* @param sm :: True to set the player to 'sniper mode'.
*/
void Player::SetSniperMode( bool sm) {
	m_sniperMode = sm;
}

/**
* Gives the player credits.
* NOTE: A minus number will deduct credits from the player.
*
* @param c :: The amount of credits to give the player.
*/
void Player::GiveCredits( s32 c) {
	m_credits += c;
}

/**
* Resets the player's state regarding their position (i.e. the mission has ended so they shouldn't
* be in sniper mode or crouching anymore.
*/
void Player::ResetState() {
	m_height = gsc_heightPositions[0];

	this->SetPositionState( PP_Standing);
	this->SetRunning( false);
	this->SetSniperMode( false);
}