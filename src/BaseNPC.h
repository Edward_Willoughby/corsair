/********************************************************************
*
*	CLASS		:: BaseNPC
*	DESCRIPTION	:: The base class for all N.P.C.s.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 08 / 16
*
********************************************************************/

#ifndef BaseNPCH
#define BaseNPCH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBERenderable.h>

namespace DBE {
	class BasicPixelShader;
	class BasicVertexShader;
}

struct VertPos3fColour4ubNormal3f;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class BaseNPC : public DBE::Renderable {
	public:
		/// Constructor.
		BaseNPC( const char* model);
		/// Destructor.
		~BaseNPC();
		
		/// Renders the N.P.C. (call 'Render').
		void OnRender( float deltaTime);

		/// Check if the N.P.C. can see the player.
		bool CanSeeThePlayer( const DBE::Movable* player);

		/// Check if the N.P.C. was hit by the player's bullet.
		bool HitByBullet( const DBE::Vec3& bulletPos, const DBE::Vec3& bulletDir);

		/// Get if the N.P.C.s is suspicious of the player.
		bool IsSuspicious() const;
		/// Get if the N.P.C. is dead.
		bool IsDead() const;

		/// Kills the N.P.C.
		void Kill();
		
	private:
		/// Loads the object's mesh.
		void LoadMesh( const char* file);
		/// Creates a '.dbm' file.
		void SaveDBMFile( const char* f);
		/// Loads a '.dbm' file.
		void LoadDBMFile( const char* f);
		/// Loads a '.obj' file.
		void LoadObjFile( const char* f);

		VertPos3fColour4ubNormal3f* mp_verts;
		UINT* mp_indicies;

		DBE::BasicVertexShader*	mp_VS;
		DBE::BasicPixelShader*	mp_PS;

		bool m_isDead;
		bool m_isSuspicious;
		
		/// Private copy constructor to prevent accidental multiple instances.
		BaseNPC( const BaseNPC& other);
		/// Private assignment operator to prevent accidental multiple instances.
		BaseNPC& operator=( const BaseNPC& other);
		
};

/*******************************************************************/
#endif	// #ifndef BaseNPCH
