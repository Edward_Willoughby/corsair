/********************************************************************
*	Function definitions for the BaseNPC class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "BaseNPC.h"

#include <fstream>
#include <list>

#include <DBEApp.h>
#include <DBEBasicPixelShader.h>
#include <DBEBasicVertexShader.h>
#include <DBEGraphicsHelpers.h>
#include <DBEMath.h>
#include <DBEShader.h>
#include <DBEVertexTypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

/// Constants for distances at which N.P.C.s can detect the player.
static const float gsc_distSuspicious = 10.0f;
static const float gsc_distDetected = gsc_distSuspicious - (gsc_distSuspicious * 0.1f);

/// Constants for the audio files used in the game.
static const char* gsc_audioSuspiciousFemale = "res/audio/sfx_suspiciousFemale.mp3";


/**
* Constructor.
*/
BaseNPC::BaseNPC( const char* model)
	: m_isDead( false)
	, m_isSuspicious( false)
{
	this->LoadMesh( model);

	// Load the shaders.
	if( !MGR_SHADER().GetShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS)) {
		mp_VS = new BasicVertexShader();
		mp_VS->Init();
		MGR_SHADER().AddShader<BasicVertexShader>( gsc_basicVertexShaderName, mp_VS);
	}
	
	if( !MGR_SHADER().GetShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS)) {
		mp_PS = new BasicPixelShader();
		mp_PS->Init();
		MGR_SHADER().AddShader<BasicPixelShader>( gsc_basicPixelShaderName, mp_PS);
	}
}

/**
* Destructor.
*/
BaseNPC::~BaseNPC() {
	SafeDeleteArray( mp_verts);
	SafeDeleteArray( mp_indicies);

	ReleaseCOM( mp_vertBuffer);
	ReleaseCOM( mp_indexBuffer);

	MGR_SHADER().RemoveShader<BasicVertexShader>( mp_VS);
	MGR_SHADER().RemoveShader<BasicPixelShader>( mp_PS);
}

/**
* Renders the N.P.C. (call 'Render').
*
* @param deltaTime :: The time taken to render the previous frame.
*/
void BaseNPC::OnRender( float deltaTime) {
	// Don't try to render if the vertex or index buffers are invalid, the shader isn't loaded, or
	// if there are too few polygons.
	if( mp_vertBuffer == nullptr || mp_indexBuffer == nullptr || mp_VS == nullptr || mp_PS == nullptr || m_indexCount < 3)
		return;

	ID3D11DeviceContext* p_context( GET_APP()->mp_D3DDeviceContext);

	float c[4];
	ColourToFloats( c[0], c[1], c[2], c[3], this->GetColour());
	mp_VS->SetVars( &this->GetWorldMatrix( false), &Vec4( c[0], c[1], c[2], c[3]));
	
	if( mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars]) {
		ID3D11Buffer* p_constBuffers[] = { mp_VS->mp_CBuffers[mp_VS->m_slotCBufferGlobalVars] };
		p_context->VSSetConstantBuffers( mp_VS->m_slotCBufferGlobalVars, 1, p_constBuffers);
	}
	
	p_context->VSSetShader( mp_VS->mp_VS, nullptr, 0);
	p_context->PSSetShader( mp_PS->mp_PS, nullptr, 0);

	p_context->IASetInputLayout( mp_VS->mp_IL);
	p_context->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ID3D11Buffer* ap_vertBuffer[1]	= { mp_vertBuffer };
	UINT aVertStride[1]				= { m_vertexStride };
	UINT aOffset[1]					= { 0 };
	p_context->IASetVertexBuffers( 0, 1, ap_vertBuffer, aVertStride, aOffset);

	p_context->IASetIndexBuffer( mp_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	p_context->DrawIndexed( m_indexCount, 0, 0);
}

/**
* Check if the N.P.C. can see the player.
*
* @param player :: Pointer to the player.
*
* @return True if the N.P.C. can see the player.
*/
bool BaseNPC::CanSeeThePlayer( const DBE::Movable* player) {
	float dist( Distance( this->GetPosition(), player->GetPosition()));

	if( dist <= gsc_distDetected) {
		return true;
	}
	else if( dist <= gsc_distSuspicious) {
		Vec3 r( this->GetRotation());
		Matrix4 rot = XMMatrixRotationRollPitchYaw( r.GetX(), r.GetY() + DBE_ToRadians( 90.0f), r.GetZ());
		Vec3 dir = XMVector3Transform( this->GetDirection().GetNormalised().GetXMVector(), rot);
		Vec3 dirToPlayer( this->GetPosition() - player->GetPosition());

		float angle( DBE_Cos( Dot( dirToPlayer, dir)));

		this->RotateBy( 0.0f, angle, 0.0f);

		if( !m_isSuspicious)
			MGR_AUDIO().PlayAudioOneShot( gsc_audioSuspiciousFemale);

		m_isSuspicious = true;
	}
	else {
		this->RotateTo( DBE_ToRadians( -90.0f), 0.0f, 0.0f);
		m_isSuspicious = false;
	}

	return false;
}

/**
* Check if the N.P.C. was hit by the player's bullet.
*
* @param bulletPos :: The bullet's position.
* @param bulletDir :: The bullet's direction.
*
* @return True if the N.P.C. was hit.
*/
bool BaseNPC::HitByBullet( const DBE::Vec3& bulletPos, const DBE::Vec3& bulletDir) {
	bool picked( false);
	float tmin( DBE_Infinity);

	for( u32 i( 0); i < m_indexCount / 3; ++i) {
		// Indicies for this triangle.
		u32 i0 = mp_indicies[i*3+0];
		u32 i1 = mp_indicies[i*3+1];
		u32 i2 = mp_indicies[i*3+2];

		// Verticies for this triangle.
		Vec3 v0 = mp_verts[i0].pos;
		Vec3 v1 = mp_verts[i1].pos;
		Vec3 v2 = mp_verts[i2].pos;

		float t( 0.0f);
		if( Collision::IntersectRayTriangle( bulletPos, bulletDir, v0, v1, v2, &t)) {
			if( t < tmin) {
				tmin = t;
				picked = true;
			}
		}
	}

	return picked;
}

/**
* Get if the N.P.C.s is suspicious of the player.
*
* @return True if the N.P.C. is suspicious.
*/
bool BaseNPC::IsSuspicious() const {
	return m_isSuspicious;
}

/**
* Get if the N.P.C. is dead.
*
* @return True if the N.P.C. is dead.
*/
bool BaseNPC::IsDead() const {
	return m_isDead;
}

/**
* Kills the N.P.C.
*/
void BaseNPC::Kill() {
	m_isDead = true;
}

/**
* Loads the object's mesh.
*
* @param file :: The name of the file load.
*/
void BaseNPC::LoadMesh( const char* file) {
	char fileName[256];
	sprintf_s( fileName, sizeof( fileName), "%s%s", file, ".dbm");

	m_vertexStride = sizeof( VertPos3fColour4ubNormal3f);

	std::fstream fin( fileName, std::ios::in);
	if( fin.is_open()) {
		fin.close();
		this->LoadDBMFile( fileName);
	}
	else {
		sprintf_s( fileName, sizeof( fileName), "%s%s", file, ".obj");
		this->LoadObjFile( fileName);

		sprintf_s( fileName, sizeof( fileName), "%s%s", file, ".dbm");
		this->SaveDBMFile( fileName);
	}

	DBE_Assert( m_vertexCount != 0 && m_indexCount != 0);

	mp_vertBuffer = CreateImmutableVertexBuffer( GET_APP()->mp_D3DDevice, sizeof( VertPos3fColour4ubNormal3f) * m_vertexCount, mp_verts);
	mp_indexBuffer = CreateImmutableIndexBuffer( GET_APP()->mp_D3DDevice, sizeof( UINT) * m_indexCount, mp_indicies);
}

/**
* Creates a '.dbm' file.
*
* @param f :: The name of the file to save to.
*/
void BaseNPC::SaveDBMFile( const char* f) {
	s32 byteCount( 0);
	char* output( nullptr);
	std::ofstream file( f, std::ios::out | std::ios::binary);

	// Vertex count and the verticies.
	byteCount = m_vertexCount * m_vertexStride;
	output = reinterpret_cast<char*>( mp_verts);

	file << m_vertexCount << "\n";
	file.write( output, byteCount);
	file << "\n";

	// Index count and the indicies.
	byteCount = m_indexCount * sizeof( UINT);
	output = reinterpret_cast<char*>( mp_indicies);

	file << m_indexCount << "\n";
	file.write( output, byteCount);
	file << "\n";

	file.close();
}

/**
* Loads a '.dbm' file.
*
* @param f :: The name of the file load.
*/
void BaseNPC::LoadDBMFile( const char* f) {
	s32 byteCount( 0);
	s32 bytesRead( 0);
	char buffer[256];
	char* data( nullptr);
	std::ifstream file( f, std::ios::in | std::ios::binary);

	file >> buffer;
	m_vertexCount = atoi( buffer);
	file.ignore( 1);
	
	// Create the array to hold the vertex data.
	mp_verts = new VertPos3fColour4ubNormal3f[m_vertexCount];
	data = reinterpret_cast<char*>( mp_verts);
	byteCount = m_vertexCount * m_vertexStride;
	bytesRead = 0;

	// Load in the verticies.
	file.read( data, byteCount);
	DBE_AssertWithMsg( file.gcount() == byteCount, "Error: Failed to read in %i bytes for the verticies.", byteCount);
	file.ignore( 1);

	file >> buffer;
	m_indexCount = atoi( buffer);
	file.ignore( 1);

	// Create the array to hold the index data.
	mp_indicies = new UINT[m_indexCount];
	data = reinterpret_cast<char*>( mp_indicies);
	byteCount = m_indexCount * sizeof( UINT);
	bytesRead = 0;

	// Load in the indicies.
	file.read( data, byteCount);
	DBE_AssertWithMsg( file.gcount() == byteCount, "Error: Failed to read in %i bytes for the indicies.", byteCount);

	file.close();
}

/**
* Loads an '.obj' file.
*
* @param f :: The name of the file load.
*/
void BaseNPC::LoadObjFile( const char* f) {
	std::vector<VertPos3fColour4ubNormal3f> verts;
	std::vector<UINT> indicies;

	bool required( true);
	u32 normalCount( 0);
	char buffer[256];

	std::fstream file( f, std::ios::in);
	DBE_AssertWithMsg( file.is_open(), "Error: Model '%s' doesn't exist.", f);

	while( required) {
		file >> buffer;

		if( buffer[0] == 'v' && strlen( buffer) == 1) {
			float pos[3];

			for( u8 i( 0); i < 3; ++i)
				file >> pos[i];

			verts.push_back( VertPos3fColour4ubNormal3f( Vec3( pos[0], pos[1], pos[2]), WHITE, Vec3YAxis));
		}
		//else if( buffer[0] == 'v' && buffer[1] == 'n') {
		//	if( normalCount >= verts.size())
		//		continue;

		//	float normal[3];

		//	for( u8 i( 0); i < 3; ++i)
		//		file >> normal[i];

		//	verts[normalCount].normal = Vec3( normal[0], normal[1], normal[2]);
		//	++normalCount;
		//}
		else if( buffer[0] == 'f') {
			char* next_token;
			file.getline( buffer, 256);

			if( buffer[0] == ' ')
				strcpy_s( buffer, 256, &buffer[1]);

			for( u8 i( 0); i < 3; ++i) {
				strtok_s( buffer, "/", &next_token);

				indicies.push_back( atoi( buffer) - 1);

				strtok_s( next_token, " ", &next_token);
				strcpy_s( buffer, 256, next_token);
			}
		}
		else if( strlen( buffer) == 0) {
			required = false;
		}
		else {
			file.getline( buffer, 256);
		}
	}
	file.close();

	m_vertexCount = verts.size();
	m_indexCount = indicies.size();

	mp_verts = new VertPos3fColour4ubNormal3f[m_vertexCount];
	mp_indicies = new UINT[m_indexCount];

	u32 counter( 0);
	for( std::vector<VertPos3fColour4ubNormal3f>::iterator it( verts.begin()); it != verts.end(); ++it, ++counter)
		mp_verts[counter] = *it;
	counter = 0;
	for( std::vector<UINT>::iterator it( indicies.begin()); it != indicies.end(); ++it, ++counter)
		mp_indicies[counter] = (*it);

	// Calculate the normals.
	Vec3* vertNormals = new Vec3[m_vertexCount];
	for( u32 i( 0); i < m_vertexCount; ++i) {
		s32 count( 0);

		for( u32 j( 0); j < m_indexCount; j += 3) {
			if( indicies[j+0] == i || indicies[j+1] == i || indicies[j+2] == i) {
				Vec3 v1( mp_verts[indicies[j+0]].pos - mp_verts[indicies[j+1]].pos);
				Vec3 v2( mp_verts[indicies[j+1]].pos - mp_verts[indicies[j+2]].pos);
				vertNormals[i] += Cross( v1, v2);
				++count;
			}
		}

		vertNormals[i] /= float( count);
		vertNormals[i].Normalise();
	}

	for( u32 i( 0); i < m_vertexCount; ++i)
		mp_verts[i].normal = vertNormals[i];

	SafeDelete( vertNormals);
}