/********************************************************************
*	Function definitions for the Corsair class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Corsair.h"

#include <DBECamera.h>

#include <InterfaceContainer.h>
#include <InterfaceObject.h>

#include "BaseNPC.h"
#include "Landscape.h"
#include "Player.h"
/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;

static u32 gs_frames = 0;
static float gs_deltaTotal = 0.0f;

/// Constant for the credit cost of an emergency extraction from a failed mission.
static const s32 gsc_creditPenalty = -500;

/// Constants for the audio files used in the game.
static const char* gsc_audioSniperShot = "res/audio/sfx_sniperShot.mp3";


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool Corsair::HandleInit() {
	m_windowTitle = "Corsair";
	this->SetWindowTitle( m_windowTitle);

	m_wireframe = false;

	m_state = GS_Count;

	mp_cam = new Camera();
	mp_cam->m_position = Vec3( 0.0f, 18.0f, -12.0f);
	mp_cam->m_lookAt = Vec3( 0.0f, 0.0f, -3.0f);

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 0.75f, 0.75f, 0.75f));

	// Add the game-specific Lua functions.
	MGR_LUA().RegisterFunction( "StartNewGame", LuaFunction_NewGame);
	MGR_LUA().RegisterFunction( "ExitGame", LuaFunction_Exit);
	MGR_LUA().RegisterFunction( "SelectMissionOne", LuaFunction_SelectMissionOne);
	MGR_LUA().RegisterFunction( "SelectMissionTwo", LuaFunction_SelectMissionTwo);
	MGR_LUA().RegisterFunction( "SelectMissionThree", LuaFunction_SelectMissionThree);
	MGR_LUA().RegisterFunction( "SelectMission", LuaFunction_SelectMission);
	MGR_LUA().RegisterFunction( "GoToShipMissionSelect", LuaFunction_GoToShipMissionSelect);

	mp_interface = new InterfaceContainer();
	if( !mp_interface->Init( this, InterfaceFuncPointers()))
		return false;

	if( !this->SetGameState( GameState::GS_Menu))
		return false;

	mp_player = new Player();
	mp_landscape = nullptr;
	mp_target = nullptr;

	// Setup the missions.
	u32 sizeName( sizeof( m_missions[0].m_name));
	u32 sizeDesc( sizeof( m_missions[0].m_desc));

	sprintf_s( m_missions[0].m_name, sizeName, "The Girl");
	sprintf_s( m_missions[0].m_desc, sizeName, "Contract.\n\nAssassinate the girl from afar.\nEnsure you are not seen.");
	m_missions[0].m_reward = 1500;
	sprintf_s( m_missions[1].m_name, sizeName, "Placeholder");
	sprintf_s( m_missions[1].m_desc, sizeName, "Not implemented yet.");
	m_missions[1].m_reward = 0;
	sprintf_s( m_missions[2].m_name, sizeName, "Placeholder Too");
	sprintf_s( m_missions[2].m_desc, sizeName, "Not implemented yet either (get off my back, dammit!).");
	m_missions[2].m_reward = 0;

	MGR_AUDIO().SetMasterVolume( 0.3f);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool Corsair::HandleInput() {
#ifdef _DEBUG
	// Toggle debug input.
	if( MGR_INPUT().KeyPressed( VK_TAB))
		m_debugMode = !m_debugMode;

	if( m_debugMode) {
		// Toggle wireframe mode.
		if( MGR_INPUT().KeyPressed( 'W'))
			m_wireframe = !m_wireframe;

		static bool s_capFPS( true);
		if( MGR_INPUT().KeyPressed( 'F')) {
			s_capFPS = !s_capFPS;

			if( s_capFPS)
				FRAME_TIMER().SetFPS( true, 60);
			else
				FRAME_TIMER().SetFPS( false);
		}

		// Don't track any other input if it's in debug mode.
		return true;
	}
#endif	// #ifdef _DEBUG

	// Quit the program.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

	// Update the game state.
	if( m_state != m_stateNew)
		if( !this->SetGameState( m_stateNew))
			return false;

	switch( m_state) {
		case GS_Play:
			// Player looking.
			Vec2 mousePos;
			if( MGR_INPUT().GetMousePos( mousePos)) {
				Vec2 dist( mousePos - Vec2( (float)HALF_SCREEN_WIDTH, (float)HALF_SCREEN_HEIGHT));

				if( mousePos != Vec2( 0.0f)) {
					dist /= mp_player->GetMouseSensitivity();
					mp_player->RotateBy( -dist.GetY(), dist.GetX(), 0.0f);
				}
			}

			// Player crouching/lying.
			if( MGR_INPUT().KeyPressed( VK_CONTROL))
				mp_player->SetPositionState( PP_Crouching);
			else if( MGR_INPUT().KeyPressed( 'C'))
				mp_player->SetPositionState( PP_Lying);

			// Player running.
			mp_player->SetRunning( MGR_INPUT().KeyHeld( VK_LSHIFT));

			// Player movement.
			float mov( mp_player->GetMoveSpeed());
			if( MGR_INPUT().KeyHeld( 'W'))
				mp_player->MoveForward( mov);
			if( MGR_INPUT().KeyHeld( 'S'))
				mp_player->MoveBack( mov);
			if( MGR_INPUT().KeyHeld( 'A'))
				mp_player->MoveLeft( mov);
			if( MGR_INPUT().KeyHeld( 'D'))
				mp_player->MoveRight( mov);

			// Sniper zoom.
			if( MGR_INPUT().KeyPressed( VK_RBUTTON)) {
				if( mp_player->IsInSniperMode())
					mp_player->SetSniperMode( false);
				else if( mp_player->GetPositionState() == PP_Lying)
					mp_player->SetSniperMode( true);
			}

			// Fire bullet.
			if( MGR_INPUT().KeyPressed( VK_LBUTTON)) {
				if( mp_player->IsInSniperMode()) {
					MGR_AUDIO().PlayAudioOneShot( gsc_audioSniperShot);

					Vec2 mousePos;
					if( MGR_INPUT().GetMousePos( mousePos)) {
						Vec3 bulletPos, bulletDir;
						Collision::ConvertOrthographicPosToRay( mousePos, mp_target->GetWorldMatrix(), bulletPos, bulletDir);
						
						if( mp_target->HitByBullet( bulletPos, bulletDir)) {
							mp_target->Kill();
							this->SetGameState( GS_LevelComplete);
						}
					}
				}
			}

			// Keep the mouse in the center of the screen.
			if( this->IsInFocus())
				MGR_INPUT().SetMousePos( Vec2( (float)HALF_SCREEN_WIDTH, (float)HALF_SCREEN_HEIGHT));
			break;
	}

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool Corsair::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	switch( m_state) {
		case GS_Menu:
			break;

		case GS_Play:
			// Check if the player has been seen by the target.
			if( mp_target->CanSeeThePlayer( mp_player))
				this->SetGameState( GS_LevelFailed, true);

			mp_landscape->Update( deltaTime);

			this->StickObjectsToLandscape();
			mp_player->Update( deltaTime);

			mp_target->Update( deltaTime);
			break;

		case GS_LevelComplete:
			break;

		case GS_LevelFailed:
			break;
	}

	mp_cam->Update();

	mp_interface->Update( deltaTime);

	++gs_frames;
	gs_deltaTotal += deltaTime;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool Corsair::HandleRender( float deltaTime) {
	this->HandleRender3D( deltaTime);
	this->HandleRender2D( deltaTime);

	// Re-apply the 3D projection matrix and view matrix so they're stored for the next update and
	// can thus be used for 'picking'.
	mp_cam->ApplyPerspectiveMatrixLH( 0.1f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	return true;
}

/**
* Deals with any 3D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void Corsair::HandleRender3D( float deltaTime) {
	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	switch( m_state) {
		case GS_Play:
		case GS_LevelComplete:
		case GS_LevelFailed:
			this->SetCameraPosition();
			mp_cam->ApplyPerspectiveMatrixLH( 0.1f, 1000.0f);
			mp_cam->ApplyViewMatrixLH();

			mp_landscape->Render( deltaTime);

			mp_target->Render( deltaTime);
			break;
	}
}

/**
* Deals with any 2D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void Corsair::HandleRender2D( float deltaTime) {
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 1000.0f);

	GET_APP()->SetBlendState( true);
	GET_APP()->SetDepthStencilState( false, true);
	GET_APP()->SetRasteriserState( false, m_wireframe);

	switch( m_state) {
		case GS_Play:
			mp_player->Render2D( deltaTime);
			break;

		case GS_LevelComplete:
			//DEFAULT_FONT()->DrawString2D( Vec3( -0.1f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Success! You assassinated the target!");
			break;

		case GS_LevelFailed:
			//DEFAULT_FONT()->DrawString2D( Vec3( -0.1f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Failure! You were detected!");
			break;
	}

	mp_interface->Render( deltaTime);

	// Debug text.
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Picked: %i", mp_level->GetHoveredTile());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Highlight: %f", mp_level->GetHoveredTileHightlight());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.225f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Building Hovered: %i", mp_ui->GetSelectedOption());
	
	//Vec2 pos, screen;
	//if( MGR_INPUT().GetMousePos( pos)) {
	//	if( pos.GetX() != -1 && pos.GetY() != -1) {
	//		screen.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
	//		screen.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);
	//	}
	//}
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", pos.GetX(), pos.GetY());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.100f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", screen.GetX(), screen.GetY());
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool Corsair::HandleShutdown() {
	SafeDelete( mp_cam);

	mp_interface->Shutdown();
	SafeDelete( mp_interface);

	SafeDelete( mp_landscape);

	SafeDelete( mp_player);
	SafeDelete( mp_target);

	DebugTrace( "Frames: %i\nDelta Average: %f\nAverage F.P.S.: %f\n", gs_frames, gs_deltaTotal / gs_frames, gs_frames / gs_deltaTotal);

	return true;
}

/**
* Sets the new game state and performs any immediate operations.
*
* @param s				:: The new state for the game.
* @param onNextUpdate	:: Specifies if the state change should happen at the start of the next
*							frame (this is to prevent errors occurring during Lua scripts).
*
* @return True if no errors occurred.
*/
bool Corsair::SetGameState( GameState s, bool onNextUpdate /*= false*/) {
	if( m_state == s)
		return true;

	if( onNextUpdate) {
		m_stateNew = s;
		return true;
	}

	// Exit the current state.
	switch( m_state) {
		case GS_Menu:
			mp_interface->DeleteAllObjects();
			break;

		case GS_ShipMissionSelect:
			mp_interface->DeleteAllObjects();
			break;

		case GS_Play:
			this->ShowCursor();
			break;

		case GS_LevelComplete:
		case GS_LevelFailed:
			mp_cam->AttachToObject( nullptr);

			mp_interface->DeleteAllObjects();

			mp_player->ResetState();

			SafeDelete( mp_landscape);
			SafeDelete( mp_target);
			break;
	}

	// Enter the new state.
	switch( s) {
		case GS_Menu:
			if( !mp_interface->LoadInterface( "res/interfaces/interface_menuMain.dbif"))
				return false;
			break;

		case GS_ShipMissionSelect: {
			if( !mp_interface->LoadInterface( "res/interfaces/interface_shipMissionSelect.dbif"))
				return false;

			char credits[256];
			sprintf_s( credits, sizeof( credits), "Credits: �%i", mp_player->GetCredits());
			mp_interface->GetObjectByName( "textCredits")->SetText( credits);

			mp_interface->GetObjectByName( "btnMissionOne")->SetText( m_missions[0].m_name);
			mp_interface->GetObjectByName( "btnMissionTwo")->SetText( m_missions[1].m_name);
			mp_interface->GetObjectByName( "btnMissionThree")->SetText( m_missions[2].m_name);

			this->DisplayMissionInfo( 0);
			break;
								   }

		case GS_Play: {
			LandscapeSettings ls;
			ls.height = 50;
			ls.width = 50;

			mp_landscape = new Landscape();
			mp_landscape->SetLandscapeSettings( ls);
			mp_landscape->GenerateLandscape();

			mp_player->MoveTo( 20.0f, 0.0f, 5.0f);
			mp_cam->AttachToObject( mp_player);
			mp_cam->m_zDist = 0.1f;

			mp_target = new BaseNPC( "res/models/girl");
			mp_target->RotateBy( DBE_ToRadians( -90.0f), 0.0f, 0.0f);
			mp_target->ScaleTo( 0.15f);

			this->ShowCursor( false);
			break;
					  }

		case GS_LevelComplete: {
		case GS_LevelFailed:
			if( !mp_interface->LoadInterface( "res/interfaces/interface_missionEnd.dbif"))
				return false;

			char status[256];
			char reward[256];
			s32 credits( 0);
			if( s == GS_LevelComplete) {
				sprintf_s( status, sizeof( status), "Mission Accomplished!");
				sprintf_s( reward, sizeof( reward), "Reward: �%i", m_missions[m_selectedMission].m_reward);
				credits = m_missions[m_selectedMission].m_reward;
			}
			else {
				sprintf_s( status, sizeof( status), "Mission Failed!");
				sprintf_s( reward, sizeof( reward), "Emergency Extraction Fee: -�%i", gsc_creditPenalty * -1);
				credits = gsc_creditPenalty;
			}

			mp_interface->GetObjectByName( "missionStatus")->SetText( status);
			mp_interface->GetObjectByName( "missionReward")->SetText( reward);
			mp_player->GiveCredits( credits);
			break;
							   }
	}

	m_state = s;
	m_stateNew = s;

	return true;
}

/**
* Starts a new game.
*/
void Corsair::StartNewGame() {
	this->SetGameState( GS_ShipMissionSelect, true);
}

/**
* Displays the mission information about a specific mission.
*
* @param i :: The mission number.
*/
void Corsair::DisplayMissionInfo( s32 i) {
	m_selectedMission = i;

	char reward[256];
	sprintf_s( reward, sizeof( reward), "Reward: �%i", m_missions[i].m_reward);

	mp_interface->GetObjectByName( "missionDesc")->SetText( m_missions[i].m_desc);
	mp_interface->GetObjectByName( "missionReward")->SetText( reward);
}

/**
* Selects the mission to be played.
*/
void Corsair::SelectMission() {
	if( m_selectedMission == 0)
		this->SetGameState( GS_Play, true);
}

/**
* Sets the player's y position to that of the landscape.
*/
void Corsair::StickObjectsToLandscape() {
	Vec3 colPos;
	Vec3 origin;
	float pointAbove( 1000.0f);

	// Player.
	origin = mp_player->GetPosition();
	origin.SetY( pointAbove);
	mp_landscape->FindHeightmapPoint( origin, colPos);
	origin.SetY( colPos.GetY() + mp_player->GetHeight());
	mp_player->MoveTo( origin);

	// Target.
	origin = mp_target->GetPosition();
	origin.SetY( pointAbove);
	mp_landscape->FindHeightmapPoint( origin, colPos);
	origin.SetY( colPos.GetY());
	mp_target->MoveTo( origin);
}

/**
* Sets the camera's position based on the player's location.
*/
void Corsair::SetCameraPosition() {
	//mp_cam->m_position = mp_player->GetPosition();
	//mp_cam->m_position.SetY( mp_cam->m_position.GetY() + mp_player->GetHeight());

	//mp_cam->m_lookAt = mp_cam->m_position + mp_player->GetDirection();

	//mp_cam->m_up = Vec3( 0.0f, 1.0f, 0.0f);
}

s32 Corsair::LuaFunction_NewGame() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->StartNewGame();

	return 0;
}

s32 Corsair::LuaFunction_Exit() {
	GET_APP()->Terminate();

	return 0;
}

s32 Corsair::LuaFunction_SelectMissionOne() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->DisplayMissionInfo( 0);

	return 0;
}

s32 Corsair::LuaFunction_SelectMissionTwo() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->DisplayMissionInfo( 1);

	return 0;
}

s32 Corsair::LuaFunction_SelectMissionThree() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->DisplayMissionInfo( 2);

	return 0;
}

s32 Corsair::LuaFunction_SelectMission() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->SelectMission();

	return 0;
}

s32 Corsair::LuaFunction_GoToShipMissionSelect() {
	Corsair* p_app( dynamic_cast<Corsair*>( GET_APP()));
	p_app->SetGameState( GS_ShipMissionSelect, true);

	return 0;
}